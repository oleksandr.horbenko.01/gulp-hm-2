// const gulp = require("gulp");
// const sass = require('gulp-sass')(require('sass'));
// const autoprefixer = require("gulp-autoprefixer");
// const uglify = require('gulp-uglify');
// const cleanCSS = require('gulp-clean-css');
// const babel = require('gulp-babel');
// const browserSync = require('browser-sync').create();


// gulp.task("styles", function () {
//     return gulp.src("src/scss/**/*.scss")
//         .pipe(sass().on("error", sass.logError))
//         .pipe(autoprefixer({
//             cascade: false
//         }))
//         .pipe(cleanCSS({
//             compatibility: 'ie8'
//         }))
//         .pipe(gulp.dest("dist/css"))
//         .on("end", browserSync.reload);
// });

// gulp.task('scripts', function () {
//     return gulp.src('src/js/**/*.js')
//         .pipe(babel({
//             presets: ['@babel/env']
//         }))
//         .pipe(uglify())
//         .pipe(gulp.dest('dist/js'))
//         .pipe(browserSync.stream());
// });

// gulp.task("serve", function () {
//     browserSync.init({
//         server: "./",
//         injectChanges: true
//     });

//     gulp.watch("src/scss/**/*.scss", gulp.series("styles"));
//     gulp.watch("src/js/**/*.js", gulp.series("scripts"));
//     gulp.watch("./*.html").on("change", browserSync.reload);
// });







const gulp = require("gulp");
const sass = require("gulp-sass")(require("sass"));
const autoprefixer = require("gulp-autoprefixer");
const cleanCSS = require("gulp-clean-css");
const concat = require("gulp-concat");
const uglify = require("gulp-uglify");
const clean = require("gulp-clean");
const imagemin = require("gulp-imagemin");
const browserSync = require("browser-sync").create();

const paths = {
  src: {
    root: "src/",
    html: "src/*.html",
    styles: "src/scss/**/*.scss",
    scripts: "src/js/**/*.js",
    images: "src/img/**/*",
  },
  dist: {
    root: "dist/",
    styles: "dist/css/",
    scripts: "dist/js/",
    images: "dist/img/",
  },
};

gulp.task("clean", function () {
  return gulp.src(paths.dist.root, { read: false, allowEmpty: true }).pipe(clean());
});


gulp.task("styles", function () {
  return gulp
    .src(paths.src.styles)
    .pipe(sass().on("error", sass.logError))
    .pipe(autoprefixer())
    .pipe(cleanCSS({ compatibility: "ie8" }))
    .pipe(gulp.dest(paths.dist.styles))
    .pipe(browserSync.stream());
});


gulp.task("scripts", function () {
  return gulp
    .src(paths.src.scripts)
    .pipe(concat("scripts.min.js"))
    .pipe(uglify())
    .pipe(gulp.dest(paths.dist.scripts))
    .pipe(browserSync.stream());
});


gulp.task("images", function () {
  return gulp.src(paths.src.images).pipe(imagemin()).pipe(gulp.dest(paths.dist.images));
});


gulp.task("html", function () {
  return gulp.src(paths.src.html).pipe(gulp.dest(paths.dist.root));
});


gulp.task("watch", function () {
  gulp.watch(paths.src.styles, gulp.series("styles"));
  gulp.watch(paths.src.scripts, gulp.series("scripts"));
  gulp.watch(paths.src.images, gulp.series("images"));
  gulp.watch(paths.src.html, gulp.series("html", browserSync.reload));
});


gulp.task("serve", function () {
  browserSync.init({
    server: {
      baseDir: "./",
    },
    injectChanges: true,
  });

  gulp.series("watch")();
});


gulp.task("dev", gulp.series("clean", "styles", "scripts", "images", "html", "serve"));


gulp.task("build", gulp.series("clean", "styles", "scripts", "images", "html"));


gulp.task("default", gulp.series("dev"));