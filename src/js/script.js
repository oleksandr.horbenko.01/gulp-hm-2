const burgerMenu = document.querySelector('.burger-menu');
const buttonContent = document.querySelector('.button-content');

burgerMenu.addEventListener('click', () => {
  buttonContent.classList.toggle('active');
});